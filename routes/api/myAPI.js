const express = require('express');
const router = express.Router();

class Show {
    constructor(image, slug, title) {
      this.image = image;
      this.slug = slug;
      this.title = title;
    }
}

//drm + episode count filter
const drmFilter = req => rquestShow => (rquestShow.drm == true && rquestShow.episodeCount > 0);

//Post handlr
router.post('/', function (req, res) {
    const requestShows = req.body.payload;
    
    if (requestShows) {     //check for valid payload
        const found = requestShows.some(drmFilter(req));

        if (found) {        //check for valid search result
            const fltShows = requestShows.filter(drmFilter(req));
            const shows = [];

            fltShows.forEach((fltShow, i) => {
                if (fltShow.title && fltShow.slug && fltShow.image && fltShow.image.showImage) {        //check for complete object
                    const newShow = new Show();
                    newShow.image = fltShow.image.showImage ? fltShow.image.showImage : "Image not found.";
                    newShow.slug = fltShow.slug ? fltShow.slug : "Slug not found.";
                    newShow.title = fltShow.title ? fltShow.title : "Title not found.";
                    shows.push(newShow);
                } else {
                    res.status(400).json({ error: 'Could not decode request : Missing Property' });
                }
                
            })
            res.json({"response":shows});
        } else {
            res.status(400).json({ error: 'Could not decode request : No Matching Results Found' });
        }
    } else {
        res.status(400).json({ error: 'Could not decode request : Invalid Payload' });
    }

    next(err);
});

module.exports = router;