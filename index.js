const express = require('express');
const path = require('path');

const app = express();

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// API Routes
app.use('/api/myAPI', require('./routes/api/myAPI'));

app.use((err, req, res, next) => {
    res.status(400).json({ error: 'Could not decode request : Invalid Payload' });
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));