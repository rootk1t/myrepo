## About

-Test API create using Express - Node.JS  
-Written in ES6  
-Handles Post requests only  
-Reasonable amount of error checking  
-Nodemon for testing purposes only  

## Quick Start Guide

```bash
# Install dependencies
npm install

# Serve on localhost:5000
npm run dev
```